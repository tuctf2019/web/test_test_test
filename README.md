# Test Test Test

This chal has directory listing and an (immediate?) redirect. Currently the redirect is janky as hell and sort of works.

* Desc: There are so many tests going on right now, why don't take a deep breath and list them out before you forget one?
* Flag: `TUCTF{d0nt_l34v3_y0ur_d1r3ct0ry_h4n61n6}`
* Hint: Pages moving too fast? Look for a way to sloooow them down

## How To:
1. Look at the source code of the index page. The image is in the `/img` directory. 
2. Go to that image URL. You can remove the `TEST.jpg` from the browser path and congrats: directory listing. 
3. The other file in the directory is `TODO.txt` which mentions `flag.php`. 
4. Go to `flag.php` and it redirects to `index.html`. 
5. Use a tool like Burp to intercept it before the redirect and you can see the flag. 
