# Test Test Test -- TUCTF 2019

This challenge was a directory listing vulnerability with a twist. The first step of this challenge was recognizing the image of the calendar was in the `/img` directory. From there, viewing that directory yielded the file `TODO.txt` which mentioned the flag needed to be moved from `flag.php`. Attempting to visit there would redirect to `index.html` (which was a slow redirect due to hosting issues). Using a proxy such as Burp would allow you to find the flag in the `flag.php` file.
